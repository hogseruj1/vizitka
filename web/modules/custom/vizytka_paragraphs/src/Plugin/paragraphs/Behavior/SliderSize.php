<?php

namespace Drupal\vizytka_paragraphs\Plugin\paragraphs\Behavior;


use Drupal\Core\Annotation\Translation;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Annotation\ParagraphsBehavior;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\Entity\ParagraphsType;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs\ParagraphsBehaviorBase;

/**
 * @ParagraphsBehavior(
 *   id = "behavior_paragraphs_slider_size",
 *   label = @Translation("Slider size"),
 *   description = @Translation("You can set the width of the slider."),
 *   weight = 0,
 * )
 */

class SliderSize extends ParagraphsBehaviorBase {

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(ParagraphsType $paragraphs_type) {
    return $paragraphs_type->id() == 'slider';
  }

  /**
   * {@inheritdoc}
   */
  public function view(array &$build, Paragraph $paragraph, EntityViewDisplayInterface $display, $view_mode) {
    $class_width = $paragraph->getBehaviorSetting($this->getPluginId(), 'slider_size', 'normal');
    $build['#attributes']['class'] = 'slider-size_' . str_replace('_', '-', $class_width);
  }

  /**
   * {@inheritdoc}
   */
  public function buildBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state) {
    $form['slider_size'] = [
      '#type' => 'radios',
      '#title' => $this->t('Slider width'),
      '#description' => $this->t('Slider width relative to the screen.'),
      '#options' => [
        'normal' => $this->t('Normal width'),
        'full_size' => $this->t('Full width'),
      ],
      '#default_value' => $paragraph->getBehaviorSetting($this->getPluginId(), 'slider_size', 'normal'),
    ];

    return $form;
  }
}