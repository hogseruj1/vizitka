<?php

namespace Drupal\vizytka_ds\Plugin\Block;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Menu\MenuLinkTreeInterface;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Url;
use Drupal\vizytka_paragraphs\Plugin\paragraphs\Behavior\AnchorParagraphBehavior as Anchor;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @Block(
 *   id="menu_anchors",
 *   admin_label=@Translation("Menu Anchors")
 * )
 */
class MenuAnchors extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The menu link tree service.
   *
   * @var \Drupal\Core\Menu\MenuLinkTreeInterface
   */
  protected $menuTree;

  /**
   * Menu id.
   *
   * @var string
   */
  const MENU_NAME = 'main';

  /**
   * MenuAnchors constructor.
   *
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\Core\Menu\MenuLinkTreeInterface $menu_tree
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MenuLinkTreeInterface $menu_tree) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->menuTree = $menu_tree;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array
  $configuration, $plugin_id, $plugin_definition) {
    return new static (
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('menu.link_tree')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    $parameters = $this->menuTree->getCurrentRouteMenuTreeParameters(self::MENU_NAME);

    $level = $config['level'];
    $depth = $config['depth'];
    $parameters->setMinDepth($level);

    if ($depth > 0) {
      $parameters->setMaxDepth(min($level + $depth - 1, $this->menuTree->maxDepth()));
    }

    if ($level > 1) {
      if (count($parameters->activeTrail) >= $level) {
        $menu_trail_ids = array_reverse(array_values($parameters->activeTrail));
        $menu_root = $menu_trail_ids[$level - 1];
        $parameters->setRoot($menu_root)->setMinDepth(1);
        if ($depth > 0) {
          $parameters->setMaxDepth(min($level - 1 + $depth - 1, $this->menuTree->maxDepth()));
        }
      }
    }

    $tree = $this->menuTree->load(self::MENU_NAME, $parameters);

    $manipulators = [
      ['callable' => 'menu.default_tree_manipulators:checkAccess'],
      ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
    ];
    $tree = $this->menuTree->transform($tree, $manipulators);
    $build = $this->menuTree->build($tree);

    if (!isset($build['#items'])) {
      $build = [
        '#cache' => [
          'contexts' => ['user.permissions'],
          'tags' => ['config:system.menu.main'],
        ],
        '#sorted' => TRUE,
        '#theme' => 'menu__main',
        '#menu_name' => 'main',
        '#items' => [],
      ];
    }

    $menu_links = $this->getAnchorMenuLinks();

    $build['#items'] = isset($build['#items']) ? $this->addWeightElement($build['#items']) : [];
    $build['#items'] = array_merge($build['#items'], $menu_links);

    uasort($build['#items'], [
      '\Drupal\Component\Utility\SortArray',
      'sortByWeightElement'
    ]);

    $node = \Drupal::routeMatch()->getParameter('node');

    if (!empty($node)) {
      // Merge menu and node cache tags.
      if (empty($build['#cache']['tags'])) {
        $build['#cache']['tags'] = [];
      }
      $build['#cache']['tags'] = array_merge($build['#cache']['tags'], $node->getCacheTags());
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'level' => 1,
      'depth' => 0,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form =  parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['menu_levels'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Menu levels'),
    ];

    $options = range(0, $this->menuTree->maxDepth());
    unset($options[0]);

    $form['menu_levels']['level'] = [
      '#type' => 'select',
      '#title' => $this->t('Initial visibility level'),
      '#default_value' => $config['level'],
      '#options' => $options,
      '#description' => $this->t('The menu is only visible if the menu item for the current page is at this level or below it. Use level 1 to always display this menu.'),
      '#required' => TRUE,
    ];

    $options[0] = $this->t('Unlimited');

    $form['menu_levels']['depth'] = [
      '#type' => 'select',
      '#title' => $this->t('Number of levels to display'),
      '#default_value' => $config['depth'],
      '#options' => $options,
      '#description' => $this->t('This maximum number includes the initial level.'),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();

    return [
      $this->t('Menu level: @level', ['@level' => $config['level']]),
      $this->t('Menu depth: @depth', ['@depth' => $config['depth']]),
    ];
  }

  /**
   * Generate anchor links from paragraphs.
   *
   * @return array
   */
  protected function getAnchorMenuLinks() {
    $links = [];

    $node = \Drupal::routeMatch()->getParameter('node');

    /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
    if (isset($node->field_paragraphs)) {
      foreach ($node->field_paragraphs->referencedEntities() as $paragraph) {
        $settings = $paragraph->getBehaviorSetting(Anchor::PLUGIN_ID, Anchor::STATUS_FIELD);

        if ($settings) {
          $title = $paragraph->getBehaviorSetting(Anchor::PLUGIN_ID, Anchor::TITLE_FIELD);
          $weight = $paragraph->getBehaviorSetting(Anchor::PLUGIN_ID, Anchor::WEIGHT_FIELD);

          $links[] = [
            'is_expanded' => FALSE,
            'is_collapsed' => FALSE,
            'in_active_trail' => FALSE,
            'attributes' => new Attribute([
              'class' => [
                'anchor-menu-item',
              ],
            ]),
            'title' => $title,
            'url' => Url::fromUserInput('#' . Anchor::getParagraphId($paragraph)),
            'below' => [],
            'weight' => $weight,
          ];
        }
      }
    }
    return $links;
  }

  /**
   * Add weight element for links.
   *
   * @param array $links
   *
   * @return array
   */
  protected function addWeightElement(array $links) {
    foreach ($links as &$link) {
      /** @var \Drupal\menu_link_content\Plugin\Menu\MenuLinkContent $original_link */
      $original_link = $link['original_link'];

      $link['weight'] = $original_link->getWeight();
    }

    return $links;
  }
}