/* eslint-disable no-console */
(function ($) {
  'use strict';

  $(document).ready(function () {
    var $preloader = $('.preloader-wrapper');

    $preloader.delay(1700).fadeOut('slow');
  });

  Drupal.behaviors.slider = {
    attach: function (context, settings) {
      var opt = [
        {
          slider: $('.main-slider .field--name-field-slides'),
          options: {
            arrows: false,
            autoplay: true,
            fade: true,
            autoplaySpeed: 2000,
            infinite: true,
            slidesToScroll: 1,
            slidesToShow: 1,
            speed: 300,
            cssEase: 'linear',
            variableHeight: true
          }
        },
        {
          slider: $('.instagram-wrapper'),
          options: {
            dots: false,
            infinite: true,
            speed: 300,
            arrows: true,
            slidesToShow: 6,
            variableWidth: true,
            initialSlide: 0,
            centerMode: true
          }
        },
        {
          slider: $('.facebook-posts-block'),
          options: {
            slidesToScroll: 1,
            arrows: true,
            touchMove: true,
            slidesToShow: 3,
            cssEase: 'linear',
            variableHeight: true,
            responsive: [
              {
                breakpoint: 960,
                settings: {
                  slidesToShow: 2,
                  slidesToScroll: 1
                }
              },
              {
                breakpoint: 767,
                settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1,
                  fade: true
                }
              }
            ]
          }
        }
      ];

      for (var i = 0; i < opt.length; i++) {
        this.initialize(opt[i].slider, opt[i].options);
      }
    },
    initialize: function (slider, options) {
      slider.slick(options);
    }
  };

  Drupal.behaviors.BurgerMenu = {
    attach: function (context, settings) {
      var body = $('body', context).once();
      var options = [
        {
          burger: $('.ham', context).once(),
          blocks: $('.field--name-karabas-main-menu > .menu', context)
        }
      ];
      for (var i = 0; i < options.length; i++) {
        this.menuListener(options[i].burger, options[i].blocks, body);
      }
    },
    menuListener: function (burger, blocks, body) {
      var self = this;

      burger.on('click', function () {
        if (body.hasClass('burger--open')) {
          self.closeBurgerMenu(blocks, body);
        }
        else {
          self.openBurgerMenu(blocks, body);
        }
      });
    },
    openBurgerMenu: function (blocks, body) {
      body.addClass('burger--open');
      blocks.addClass('open').slideDown();
    },
    closeBurgerMenu: function (blocks, body) {
      body.removeClass('burger--open');
      blocks.removeClass('open').slideUp();
    }
  };

  Drupal.behaviors.overlay = {
    attach: function (context, settings) {
      this.overlayFunc('.ham', '.overlay', '.field--name-karabas-main-menu .menu-item > a', 'body', '.field--name-karabas-main-menu > .menu', context);
    },
    overlayFunc: function (btn, overlay, menupoint, body, menu, context) {
      $(overlay, context).click(function () {
        $(btn, context).click();
      });

      $(document).bind('keydown', function (e) {
        if (e.which === 27) {
          if ($(body).hasClass('burger--open')) {
            $(btn, context).click();
          }
        }
      });

      $(menupoint, context).click(function () {
        $(body).removeClass('burger--open');
        $(menu).removeClass('open');
      });
    }
  };

  Drupal.behaviors.smoothScroll = {
    attach: function (context, settings) {

      $('a[href*="#"]', context).on('click', function (event) {
        const target = $($(this).attr('href'));
        event.preventDefault();

        const elem_position = target.offset().top;
        const window_height = $(window).height();
        const y = elem_position - window_height / 5;
        console.clear();
        console.log(elem_position);

        $('html, body').animate({scrollTop: y}, 600);
      });
      /* eslint-disable */
    }
  };

  Drupal.behaviors.video = {
    youtubeAPIStatus: false,
    $iframeElement: null,
    attach(context) {
      const youtubeIframeId = 'youtube-iframe';

      $('.main-slider .field--name-field-slides', context)
        .on('afterChange', (event, slick) => {
          const $currentSlide = $('.slick-current', event.target);

          if ($currentSlide.find('.silde-type-video').length && this.youtubeAPIStatus && $(window).width() >= 920) {
            const container = $currentSlide.find('.youtube-iframe-container');
            slick.pause();
            this.$iframeElement && this.$iframeElement.remove();
            this.$iframeElement = $('<div />').attr('id', youtubeIframeId);
            $currentSlide.find('.youtube-iframe-container').append(this.$iframeElement);

            const player = new YT.Player(youtubeIframeId, {
              videoId: container.attr('data-id'),
              playerVars: {
                controls: 0,
                disablekb: 1,
                autoplay: 0,
                autohide: 1,
                modestbranding: 0,
                rel: 0,
                showinfo: 0,
                enablejsapi: 0,
                iv_load_policy: 3
              },
              events: {
                onReady: e => {
//                  this.appendPoster($currentSlide, container.attr('data-id'));
                  e.target.playVideo();
                  e.target.mute();
                  this.vidRescale();
                },
                onStateChange: e => {
                  if (e.data === YT.PlayerState.ENDED) {
                    slick.play();
                    player.destroy();
                    this.$iframeElement.remove()
                  }
                }
              }
            });
          }
      });

    },
    updateYoutubeStatus() {
      this.youtubeAPIStatus = true;
    },
    createYoutubeScript() {
      const tag = document.createElement('script');
      tag.src = `https://www.youtube.com/iframe_api`;
      const firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    },
    vidRescale() {
      var w = $('.field--name-karabas-video').width()+200,
          h = $('.field--name-karabas-video').height()+200;
      var iframe = $('#youtube-iframe');

      if (w/h > 16/9){
        iframe.width(w);
        iframe.height( w/16*9);
        iframe.css({'left': '0px'});
      } else {
        iframe.width(h/9*16);
        iframe.height(h);
        iframe.css({'left': -($('#youtube-iframe').outerWidth()-w)/2});
      }
    },
  };

  window.addEventListener('resize', () => Drupal.behaviors.video.vidRescale())

  window.onYouTubeIframeAPIReady = () => {
    Drupal.behaviors.video.updateYoutubeStatus()
  };

  Drupal.behaviors.video.createYoutubeScript()

  Drupal.behaviors.animationOnScroll = {
    attach: function (context, settings) {
      // eslint-disable-next-line no-undef
      AOS.init({
        offset: 120,
        delay: 0,
        disable: function () {
          var maxWidth = 970;
          return window.innerWidth < maxWidth;
        },
        easing: 'ease',
        duration: 400,
        once: false,
        startEvent: 'DOMContentLoaded'
      });
    }
  };

  Drupal.behaviors.inputMask = {
    attach: function (context, settings) {
      var phone = [
        {
          selector: 'input[type=tel]',
          context:context,
          options: {
            'mask': '+380 (99)99-99-999'
          }
        }
      ]

      phone.forEach(function (el) {
        $(el.selector).inputmask(el.options)
      })
    }
  };

})(jQuery);
