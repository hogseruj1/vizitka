<?php

/**
 * @file
 * Lists available colors and color schemes for the vizytka_theme theme.
 */

$info = [
  // Available colors and color labels used in theme.
  'fields' => [
    'black' => ('black'),
    'white' => ('white'),
    'brand' => ('brand'),
    'secondary' => ('secondary'),
    'third' => ('third'),
  ],
  // Pre-defined color schemes.
  'schemes' => [
    'default' => [
      'title' => ('Default Palette'),
      'colors' => [
        'black' => '#000000',
        'white' => '#ffffff',
        'brand' => '#1b1b1b',
        'secondary' => '#515151',
        'third' => '#898989',
      ],
    ],
    'light' => [
      'title' => ('Light Palette'),
      'colors' => [
        'black' => '#000000',
        'white' => '#ffffff',
        'brand' => '#e6b0ac',
        'secondary' => '#ffe6e4',
        'third' => '#8a7270',
      ],
    ],
    'green' => [
      'title' => ('Green Palette'),
      'colors' => [
        'black' => '#000000',
        'white' => '#ffffff',
        'brand' => '#6bc193',
        'secondary' => '#c3e9d5',
        'third' => '#3a644e',
      ],
    ],
    'purple' => [
      'title' => ('Purple Palette'),
      'colors' => [
        'black' => '#000000',
        'white' => '#ffffff',
        'brand' => '#3c2174',
        'secondary' => '#6f4bb9',
        'third' => '#160a2e',
      ],
    ],
    'red' => [
      'title' => ('Red Palette'),
      'colors' => [
        'black' => '#000000',
        'white' => '#ffffff',
        'brand' => '#861344',
        'secondary' => '#ed1b24',
        'third' => '#990035',
      ],
    ],
    'blue' => [
      'title' => ('Blue Palette'),
      'colors' => [
        'black' => '#000000',
        'white' => '#ffffff',
        'brand' => '#293d8d',
        'secondary' => '#071250',
        'third' => '#d6eefa',
      ],
    ],
  ],

  // CSS files (excluding @import) to rewrite with new color scheme.
  'css' => [
    'color/colors.css',
  ],

  'preview_library' => 'vizytka_theme/color.preview',
  'preview_html' => 'color/preview.html',

  // Files to copy.
  'copy' => [
    'logo.svg',
  ],
];