const autoprefixer = require('autoprefixer');
const postcssExtend = require('postcss-extend');
const postcssDiscardEmpty = require('postcss-discard-empty');
const postcssImport = require('postcss-import');
const postCssDrupalBreakpoints = require('@skilld/postcss-drupal-breakpoints');
const postcssNested = require('postcss-nested');
const stylelint = require('stylelint');
const tailwind = require('tailwindcss');

module.exports = () => ({
  map: false,
  plugins: [
    postcssImport(),
    tailwind(),
    postCssDrupalBreakpoints({
      importFrom: './vizytka_theme.breakpoints.yml',
      themeName: 'vizytka_theme'
    }),
    postcssNested(),
    postcssExtend(),
    autoprefixer({
      cascade: false,
      grid: 'no-autoplace',
    }),
    postcssDiscardEmpty(),
    stylelint({
      configFile: './.stylelintrc',
      fix: true
    }),
  ],
});
