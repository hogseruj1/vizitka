import { storiesOf } from '@storybook/html';
import './glide-dot.css';
import glide-dot from './a-glide-dot';

const template = require('./a-glide-dot.html.twig');
const data = require('./a-glide-dot.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();


storiesOf('atoms|glide-dot', module).add('default', () => {
  document.addEventListener(
    'DOMNodeInserted',
    () => {
      glide-dot();
    },
    false,
  );
  return template(data);
});

