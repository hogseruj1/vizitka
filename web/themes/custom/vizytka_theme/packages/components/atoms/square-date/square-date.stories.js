import { storiesOf } from '@storybook/html';
import './square-date.css';
import square-date from './a-square-date';

const template = require('./a-square-date.html.twig');
const data = require('./a-square-date.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();


storiesOf('atoms|square-date', module).add('default', () => {
  document.addEventListener(
    'DOMNodeInserted',
    () => {
      square-date();
    },
    false,
  );
  return template(data);
});

