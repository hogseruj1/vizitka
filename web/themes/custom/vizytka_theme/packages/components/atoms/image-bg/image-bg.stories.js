import { storiesOf } from '@storybook/html';
import './image-bg.css';
import image-bg from './a-image-bg';

const template = require('./a-image-bg.html.twig');
const data = require('./a-image-bg.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();


storiesOf('atoms|image-bg', module).add('default', () => {
  document.addEventListener(
    'DOMNodeInserted',
    () => {
      image-bg();
    },
    false,
  );
  return template(data);
});

