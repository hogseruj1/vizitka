import { storiesOf } from '@storybook/html';
import './site-logo.css';
import site-logo from './a-site-logo';

const template = require('./a-site-logo.html.twig');
const data = require('./a-site-logo.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();


storiesOf('atoms|site-logo', module).add('default', () => {
  document.addEventListener(
    'DOMNodeInserted',
    () => {
      site-logo();
    },
    false,
  );
  return template(data);
});

