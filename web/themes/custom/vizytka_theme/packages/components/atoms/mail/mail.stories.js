import { storiesOf } from '@storybook/html';
import './mail.css';
import mail from './a-mail';

const template = require('./a-mail.html.twig');
const data = require('./a-mail.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();


storiesOf('atoms|mail', module).add('default', () => {
  document.addEventListener(
    'DOMNodeInserted',
    () => {
      mail();
    },
    false,
  );
  return template(data);
});

