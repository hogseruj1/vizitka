import { storiesOf } from '@storybook/html';
import './text.css';
import text from './a-text';

const template = require('./a-text.html.twig');
const data = require('./a-text.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();


storiesOf('atoms|text', module).add('default', () => {
  document.addEventListener(
    'DOMNodeInserted',
    () => {
      text();
    },
    false,
  );
  return template(data);
});

