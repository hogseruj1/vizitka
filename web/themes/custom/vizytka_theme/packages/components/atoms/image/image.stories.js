import { storiesOf } from '@storybook/html';
import './image.css';
import image from './a-image';

const template = require('./a-image.html.twig');
const data = require('./a-image.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();


storiesOf('atoms|image', module).add('default', () => {
  document.addEventListener(
    'DOMNodeInserted',
    () => {
      image();
    },
    false,
  );
  return template(data);
});

