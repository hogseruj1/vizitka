import { storiesOf } from '@storybook/html';
import './title.css';
import title from './a-title';

const template = require('./a-title.html.twig');
const data = require('./a-title.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();


storiesOf('atoms|title', module).add('default', () => {
  document.addEventListener(
    'DOMNodeInserted',
    () => {
      title();
    },
    false,
  );
  return template(data);
});

