import { storiesOf } from '@storybook/html';
import './menu.css';
import menu from './a-menu';

const template = require('./a-menu.html.twig');
const data = require('./a-menu.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();


storiesOf('atoms|menu', module).add('default', () => {
  document.addEventListener(
    'DOMNodeInserted',
    () => {
      menu();
    },
    false,
  );
  return template(data);
});

