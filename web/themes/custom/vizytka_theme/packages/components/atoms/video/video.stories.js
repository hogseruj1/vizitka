import { storiesOf } from '@storybook/html';
import './video.css';
import video from './a-video';

const template = require('./a-video.html.twig');
const data = require('./a-video.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();


storiesOf('atoms|video', module).add('default', () => {
  document.addEventListener(
    'DOMNodeInserted',
    () => {
      video();
    },
    false,
  );
  return template(data);
});

