import { storiesOf } from '@storybook/html';
import './overlay.css';
import overlay from './a-overlay';

const template = require('./a-overlay.html.twig');
const data = require('./a-overlay.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();


storiesOf('atoms|overlay', module).add('default', () => {
  document.addEventListener(
    'DOMNodeInserted',
    () => {
      overlay();
    },
    false,
  );
  return template(data);
});

