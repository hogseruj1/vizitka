import { storiesOf } from '@storybook/html';
import './social.css';
import social from './a-social';

const template = require('./a-social.html.twig');
const data = require('./a-social.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();


storiesOf('atoms|social', module).add('default', () => {
  document.addEventListener(
    'DOMNodeInserted',
    () => {
      social();
    },
    false,
  );
  return template(data);
});

