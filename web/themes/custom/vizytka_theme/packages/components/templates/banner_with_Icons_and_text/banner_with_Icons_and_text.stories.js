import { storiesOf } from '@storybook/html';
import './banner_with_Icons_and_text.css';
import banner_with_Icons_and_text from './t-banner_with_Icons_and_text';

const template = require('./t-banner_with_Icons_and_text.html.twig');
const data = require('./t-banner_with_Icons_and_text.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();


storiesOf('templates|banner_with_Icons_and_text', module).add('default', () => {
  document.addEventListener(
    'DOMNodeInserted',
    () => {
      banner_with_Icons_and_text();
    },
    false,
  );
  return template(data);
});

