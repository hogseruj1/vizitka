import { storiesOf } from '@storybook/html';
import './item-contact.css';
import item-contact from './m-item-contact';

const template = require('./m-item-contact.html.twig');
const data = require('./m-item-contact.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();


storiesOf('molecules|item-contact', module).add('default', () => {
  document.addEventListener(
    'DOMNodeInserted',
    () => {
      item-contact();
    },
    false,
  );
  return template(data);
});

