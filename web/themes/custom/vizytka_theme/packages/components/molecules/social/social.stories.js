import { storiesOf } from '@storybook/html';
import './social.css';
import social from './m-social';

const template = require('./m-social.html.twig');
const data = require('./m-social.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();


storiesOf('molecules|social', module).add('default', () => {
  document.addEventListener(
    'DOMNodeInserted',
    () => {
      social();
    },
    false,
  );
  return template(data);
});

