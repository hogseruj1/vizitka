import { storiesOf } from '@storybook/html';
import './item-info.css';
import item-info from './m-item-info';

const template = require('./m-item-info.html.twig');
const data = require('./m-item-info.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();


storiesOf('molecules|item-info', module).add('default', () => {
  document.addEventListener(
    'DOMNodeInserted',
    () => {
      item-info();
    },
    false,
  );
  return template(data);
});

