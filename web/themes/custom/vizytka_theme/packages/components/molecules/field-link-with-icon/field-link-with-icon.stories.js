import { storiesOf } from '@storybook/html';
import './field-link-with-icon.css';
import field-link-with-icon from './m-field-link-with-icon';

const template = require('./m-field-link-with-icon.html.twig');
const data = require('./m-field-link-with-icon.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();


storiesOf('molecules|field-link-with-icon', module).add('default', () => {
  document.addEventListener(
    'DOMNodeInserted',
    () => {
      field-link-with-icon();
    },
    false,
  );
  return template(data);
});

