import { storiesOf } from '@storybook/html';
import './paragraph--type--number-with-text.css';
import paragraph--type--number-with-text from './m-paragraph--type--number-with-text';

const template = require('./m-paragraph--type--number-with-text.html.twig');
const data = require('./m-paragraph--type--number-with-text.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();


storiesOf('molecules|paragraph--type--number-with-text', module).add('default', () => {
  document.addEventListener(
    'DOMNodeInserted',
    () => {
      paragraph--type--number-with-text();
    },
    false,
  );
  return template(data);
});

