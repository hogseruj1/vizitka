import { storiesOf } from '@storybook/html';
import './slider-slide.css';
import slider-slide from './m-slider-slide';

const template = require('./m-slider-slide.html.twig');
const data = require('./m-slider-slide.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();


storiesOf('molecules|slider-slide', module).add('default', () => {
  document.addEventListener(
    'DOMNodeInserted',
    () => {
      slider-slide();
    },
    false,
  );
  return template(data);
});

