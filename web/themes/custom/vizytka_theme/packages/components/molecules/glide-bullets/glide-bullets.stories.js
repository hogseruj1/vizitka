import { storiesOf } from '@storybook/html';
import './glide-bullets.css';
import glide-bullets from './m-glide-bullets';

const template = require('./m-glide-bullets.html.twig');
const data = require('./m-glide-bullets.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();


storiesOf('molecules|glide-bullets', module).add('default', () => {
  document.addEventListener(
    'DOMNodeInserted',
    () => {
      glide-bullets();
    },
    false,
  );
  return template(data);
});

