import { storiesOf } from '@storybook/html';
import './item-work.css';
import item-work from './m-item-work';

const template = require('./m-item-work.html.twig');
const data = require('./m-item-work.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();


storiesOf('molecules|item-work', module).add('default', () => {
  document.addEventListener(
    'DOMNodeInserted',
    () => {
      item-work();
    },
    false,
  );
  return template(data);
});

