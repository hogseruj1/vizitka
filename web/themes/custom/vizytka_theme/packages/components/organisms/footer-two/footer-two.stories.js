import { storiesOf } from '@storybook/html';
import './footer-two.css';


const template = require('./o-footer-two.html.twig');
const data = require('./o-footer-two.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();


storiesOf('organisms|footer-two', module).add('default', () => template(data));

