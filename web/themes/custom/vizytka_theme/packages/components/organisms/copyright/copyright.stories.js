import { storiesOf } from '@storybook/html';
import './copyright.css';
import copyright from './o-copyright';

const template = require('./o-copyright.html.twig');
const data = require('./o-copyright.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();


storiesOf('organisms|copyright', module).add('default', () => {
  document.addEventListener(
    'DOMNodeInserted',
    () => {
      copyright();
    },
    false,
  );
  return template(data);
});

