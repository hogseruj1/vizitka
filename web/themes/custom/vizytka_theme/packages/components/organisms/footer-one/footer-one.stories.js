import { storiesOf } from '@storybook/html';
import './footer-one.css';
import footer-one from './o-footer-one';

const template = require('./o-footer-one.html.twig');
const data = require('./o-footer-one.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();


storiesOf('organisms|footer-one', module).add('default', () => {
  document.addEventListener(
    'DOMNodeInserted',
    () => {
      footer-one();
    },
    false,
  );
  return template(data);
});

