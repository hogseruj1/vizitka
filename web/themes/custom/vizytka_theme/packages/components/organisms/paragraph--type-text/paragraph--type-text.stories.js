import { storiesOf } from '@storybook/html';
import './paragraph--type-text.css';
import paragraph--type-text from './o-paragraph--type-text';

const template = require('./o-paragraph--type-text.html.twig');
const data = require('./o-paragraph--type-text.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();


storiesOf('organisms|paragraph--type-text', module).add('default', () => {
  document.addEventListener(
    'DOMNodeInserted',
    () => {
      paragraph--type-text();
    },
    false,
  );
  return template(data);
});

