import { storiesOf } from '@storybook/html';
import './paragraph--type-title-banner.css';
import paragraph--type-title-banner from './o-paragraph--type-title-banner';

const template = require('./o-paragraph--type-title-banner.html.twig');
const data = require('./o-paragraph--type-title-banner.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();


storiesOf('organisms|paragraph--type-title-banner', module).add('default', () => {
  document.addEventListener(
    'DOMNodeInserted',
    () => {
      paragraph--type-title-banner();
    },
    false,
  );
  return template(data);
});

