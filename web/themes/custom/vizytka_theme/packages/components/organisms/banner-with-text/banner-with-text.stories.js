import { storiesOf } from '@storybook/html';
import './banner-with-text.css';
import banner-with-text from './o-banner-with-text';

const template = require('./o-banner-with-text.html.twig');
const data = require('./o-banner-with-text.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();


storiesOf('organisms|banner-with-text', module).add('default', () => {
  document.addEventListener(
    'DOMNodeInserted',
    () => {
      banner-with-text();
    },
    false,
  );
  return template(data);
});

