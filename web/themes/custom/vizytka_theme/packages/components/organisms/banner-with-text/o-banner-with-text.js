import { storiesOf } from '@storybook/html';
import './nav-link.css';
import svg_sprite_path from '../../../../../dist/svg/sprite.svg';

const template = require('./a-nav-link.html.twig');
const data = require('./a-nav-link.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();

storiesOf('atoms|nav-link', module)
  .add('default', () => template(data))
  .add('with icon', () =>
    template({
      ...data,
      svg_sprite_path,
    }),
  )
  .add('secondary', () =>
    template({
      ...data,
      svg_sprite_path,
      modifier_class: 'a-nav-link--secondary',
    }),
  );
