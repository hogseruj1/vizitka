import { storiesOf } from '@storybook/html';
import './paragraph--type--banner.css';
import paragraph--type--banner from './o-paragraph--type--banner';

const template = require('./o-paragraph--type--banner.html.twig');
const data = require('./o-paragraph--type--banner.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();


storiesOf('organisms|paragraph--type--banner', module).add('default', () => {
  document.addEventListener(
    'DOMNodeInserted',
    () => {
      paragraph--type--banner();
    },
    false,
  );
  return template(data);
});

