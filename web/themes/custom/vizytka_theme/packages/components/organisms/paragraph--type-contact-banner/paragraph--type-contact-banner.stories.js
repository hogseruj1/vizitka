import { storiesOf } from '@storybook/html';
import './paragraph--type-contact-banner.css';
import paragraph--type-contact-banner from './o-paragraph--type-contact-banner';

const template = require('./o-paragraph--type-contact-banner.html.twig');
const data = require('./o-paragraph--type-contact-banner.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();


storiesOf('organisms|paragraph--type-contact-banner', module).add('default', () => {
  document.addEventListener(
    'DOMNodeInserted',
    () => {
      paragraph--type-contact-banner();
    },
    false,
  );
  return template(data);
});

