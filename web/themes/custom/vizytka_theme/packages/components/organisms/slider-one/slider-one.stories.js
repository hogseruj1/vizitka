import { storiesOf } from '@storybook/html';
import './slider-one.css';
import slider-one from './o-slider-one';

const template = require('./o-slider-one.html.twig');
const data = require('./o-slider-one.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();


storiesOf('organisms|slider-one', module).add('default', () => {
  document.addEventListener(
    'DOMNodeInserted',
    () => {
      slider-one();
    },
    false,
  );
  return template(data);
});

