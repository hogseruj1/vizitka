import { storiesOf } from '@storybook/html';
import './paragraph--type--numerical-block.css';
import paragraph--type--numerical-block from './o-paragraph--type--numerical-block';

const template = require('./o-paragraph--type--numerical-block.html.twig');
const data = require('./o-paragraph--type--numerical-block.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();


storiesOf('organisms|paragraph--type--numerical-block', module).add('default', () => {
  document.addEventListener(
    'DOMNodeInserted',
    () => {
      paragraph--type--numerical-block();
    },
    false,
  );
  return template(data);
});

