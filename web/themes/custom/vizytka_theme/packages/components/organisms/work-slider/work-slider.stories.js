import { storiesOf } from '@storybook/html';
import './work-slider.css';
import work-slider from './o-work-slider';

const template = require('./o-work-slider.html.twig');
const data = require('./o-work-slider.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();


storiesOf('organisms|work-slider', module).add('default', () => {
  document.addEventListener(
    'DOMNodeInserted',
    () => {
      work-slider();
    },
    false,
  );
  return template(data);
});

