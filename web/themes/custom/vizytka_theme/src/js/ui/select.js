/**
 * @file
 * Select component.
 */

import select from '@vizytka_theme/primary-components/atoms/select/a-select';

const selectOptions = {
  containerOuter: 'vizytka_theme-select',
  containerInner: 'vizytka_theme-select__inner',
  input: 'vizytka_theme-select__input',
  inputCloned: 'vizytka_theme-select__search',
  list: 'vizytka_theme-select__list',
  listItems: 'vizytka_theme-select__list--multiple',
  listSingle: 'vizytka_theme-select__list--single',
  listDropdown: 'vizytka_theme-select__dropdown',
  item: 'vizytka_theme-select__item',
  itemSelectable: 'vizytka_theme-select__item-selectable',
  itemDisabled: 'vizytka_theme-select__item--disabled',
  itemChoice: 'vizytka_theme-select__dropdown-item',
  placeholder: 'vizytka_theme-select__placeholder',
  group: 'vizytka_theme-select__group',
  groupHeading: 'vizytka_theme-select__heading',
  button: 'vizytka_theme-select__button',
  activeState: 'vizytka_theme-select__dropdown--active',
  focusState: 'vizytka_theme-select--focus',
  openState: 'vizytka_theme-select--open',
  disabledState: 'vizytka_theme-select--disabled',
  highlightedState: 'vizytka_theme-select__dropdown-item--active',
  hiddenState: 'hidden',
  flippedState: 'vizytka_theme-select--flipped',
  loadingState: 'vizytka_theme-select--loading',
  noResults: 'has-no-results',
  noChoices: 'has-no-choices',
  selected: 'vizytka_theme-select__selected',
  icon: 'vizytka_theme-select__icon',
};

(({ behaviors }) => {
  behaviors.vizytka_themeSelects = {
    attach(context, settings) {
      select({
        selector: 'select:not(.vizytka_theme-select__input)',
        options: selectOptions,
        svgSpritePath: settings.vizytka_themeSvgSpritePath,
      });
    },
  };
})(Drupal);
