(({ behaviors }) => {
  behaviors.videoYouTube = {
    inited: false,
    attach(context) {
      if (!this.inited) {
        this.inited = true;
        this.createYoutubeScript();
        window.addEventListener('resize', () => this.vidRescale());
      }
    },
    initYoutubePlayer() {
      const youtubeIframeId = 'youtube-iframe';

      if (window.innerWidth >= 920) {
        const container = document.querySelector('.youtube-iframe-container');
        if (container) {
          container.append(this.createElement('div', { id: youtubeIframeId }));
          new YT.Player(youtubeIframeId, {
            videoId: container.dataset.id,
            playerVars: {
              controls: 0,
              playlist: container.dataset.id,
              autoplay: 1,
              loop: 1,
              disablekb: 1,
              autohide: 1,
              modestbranding: 0,
              rel: 0,
              showinfo: 0,
              enablejsapi: 0,
              frameborder: 0,
              iv_load_policy: 3,
            },
            events: {
              onReady: e => {
                e.target.playVideo();
                e.target.mute();

                this.vidRescale();
                setTimeout(this.removeImage(), 2000);
              },
            },
          });
        }
      }
    },
    createYoutubeScript() {
      const tag = document.createElement('script');
      tag.src = 'https://www.youtube.com/iframe_api';
      const firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    },
    createElement(tag, attrs) {
      const element = document.createElement(tag);

      for (const name in attrs) {
        if (attrs.hasOwnProperty(name)) {
          element.setAttribute(name, attrs[name]);
        }
      }

      return element;
    },
    vidRescale() {
      const w = window.innerWidth + 200;
      const h = window.innerWidth + 200;
      const iframe = document.querySelector('#youtube-iframe');

      if (iframe) {
        if (w / h > 16 / 9) {
          iframe.width = w;
          iframe.height = (w / 16) * 9;
          iframe.style.left = '0px';
        } else {
          iframe.width = (h / 9) * 16;
          iframe.height = h;
          iframe.style.left = -(iframe.width - w) / 2;
        }
      }
    },
    removeImage() {
      document
        .querySelector('.youtube-iframe-preview')
        .classList.add('hide-image-video');
    },
  };

  window.onYouTubeIframeAPIReady = () =>
    behaviors.videoYouTube.initYoutubePlayer();
})(Drupal);
