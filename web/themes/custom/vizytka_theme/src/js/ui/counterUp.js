import { CountUp } from 'countup.js';

const option = {
  block: '.paragraph--type--numerical-block',
  numbers: '.field--name-field-number',
  optionsCount: {
    duration: '7',
  },
};

(({ behaviors }) => {
  behaviors.CountUp = {
    isInited: false,

    attach(context) {
      const element = context.querySelector(option.block);
      if (element) {
        const numbers = element.querySelectorAll(option.numbers);
        this.processNumbers(numbers);
      }
      if (!this.isInited) {
        window.addEventListener('scroll', () => this.visible());
        this.isInited = true;
      }
    },

    processNumbers(elements) {
      elements.forEach(el => {
        if (!el.dataset.number) {
          el.dataset.number = el.innerText;
        }
      });
    },

    visible() {
      const element = document.querySelector(option.block);
      if (element) {
        const targetPosition = {
          top:
            window.pageYOffset +
            document.querySelector(option.block).getBoundingClientRect().top,
        };
        const windowPosition = {
          top: window.pageYOffset,
          bottom: window.pageYOffset + document.documentElement.clientHeight,
        };
        if (
          targetPosition.top < windowPosition.bottom &&
          !element.classList.contains('finished')
        ) {
          this.counter(
            element.querySelectorAll(option.numbers),
            option.optionsCount,
          );
          element.classList.add('finished');
        }
      }
    },

    counter(numbers, optionsCount) {
      numbers.forEach(el => {
        if (!el.classList.contains('processed')) {
          const number = +el.dataset.number;

          el.classList.add('processed');

          const countUp = new CountUp(el, number, optionsCount);
          countUp.start(() => el.classList.remove('processed'));
        }
      });
    },
  };
})(Drupal);
