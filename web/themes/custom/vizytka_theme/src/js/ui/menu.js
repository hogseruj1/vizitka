document.addEventListener('DOMContentLoaded', () => {
  const menu = document.querySelector('.region-header');
  const burger = document.createElement('div');
  burger.className = 'nav-open';
  burger.innerHTML =
    '' +
    '<span class="icon-bar one"></span>\n' +
    '<span class="icon-bar item-menu">' +
    '  <ul class="sub-menu"></ul>' +
    '</span>\n' +
    '<span class="icon-bar three"></span>';
  const overlay = document.createElement('div');
  overlay.className = 'nav-overlay';
  menu.append(burger);
  menu.append(overlay);

  const links = document.querySelectorAll('.region-header .menu-item');
  const newMenu = burger.querySelector('.sub-menu');

  links.forEach(el => {
    const item = el.cloneNode(true);
    newMenu.append(item);
  });

  burger.addEventListener('click', () => burger.classList.toggle('opened'));
  overlay.addEventListener('click', () => burger.classList.toggle('opened'));
});
