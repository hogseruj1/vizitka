['DOMContentLoaded', 'scroll'].forEach(evt =>
  document.addEventListener(
    evt,
    () => {
      const body = document.querySelector('body:not(.path-frontpage) header');
      const header = document.getElementById('header');
      const mainWrapper = document.getElementById('main-wrapper');
      const top = window.pageYOffset;

      if (body) {
        mainWrapper.style.paddingTop = `${header.offsetHeight}px`;
      }

      if (header.offsetHeight < top) {
        header.classList.add('header-bg');
      } else {
        header.classList.remove('header-bg');
      }
    },
    false,
  ),
);
