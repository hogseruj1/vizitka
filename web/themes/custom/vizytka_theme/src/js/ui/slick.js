/**
 * @file
 * This is custom carousel component.
 */
import Glide from '@glidejs/glide';

(({ behaviors }) => {
  behaviors.vizytka_themeSlider = {
    attach(context, settings) {
      const glide = new Glide('.glide', {
        gap: 0,
        type: 'carousel',
        focusAt: 0,
        perView: 3,
        autoplay: 2000,
        animationTimingFunc: 'linear',
        hoverpause: true,
        bound: true,
        breakpoints: {
          1200: {
            perView: 2,
          },
          800: {
            perView: 1,
          },
        },
      });

      glide.mount();
    },
  };
})(Drupal);
