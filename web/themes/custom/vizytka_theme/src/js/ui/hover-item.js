(({ behaviors }) => {
  behaviors.vizytka_hoverItem = {
    attach(context, settings) {
      const items = document.querySelectorAll(
        '.paragraph--type--background-text-button',
      );

      items.forEach(item => {
        item.addEventListener('mouseenter', addClass);
        item.addEventListener('mouseleave', addClass);

        item.addEventListener('mouseleave', ev => {});

        function addClass() {
          item.querySelector('.group-right').classList.toggle('hover');
        }
      });
    },
  };
})(Drupal);
