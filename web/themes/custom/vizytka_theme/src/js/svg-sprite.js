import facebook from '../../images/svg/facebook.svg';
import google from '../../images/svg/google-plus.svg';
import instagram from '../../images/svg/instagram.svg';
import twitter from '../../images/svg/twitter.svg';
import youtube from '../../images/svg/youtube.svg';

document.addEventListener('DOMContentLoaded', () => {
  const spriteBasePath = drupalSettings.vizytka_themeSvgSpritePath;

  const options = [
    {
      selector: '.paragraph--type--social-links-with-icons a',
      socials: {
        twitter,
        instagram,
        google,
        facebook,
        youtube,
      },
    },
    {
      selector: '.social-media-links--platforms a',
      socials: {
        twitter,
        instagram,
        google,
        facebook,
        youtube,
      },
    },
  ];

  options.forEach(option => {
    const links = document.querySelectorAll(option.selector);

    links.forEach(el => {
      let icon;
      for (const name in option.socials) {
        if (option.socials.hasOwnProperty(name)) {
          if (el.host.indexOf(name) !== -1) {
            icon = option.socials[name];
          }
        }
      }

      if (icon) {
        const svg = document.createElement('div');
        svg.classList.add('wrapper-svg');
        svg.innerHTML = `
          <svg aria-hidden="true">
            <use xlink:href="${spriteBasePath}#${
          icon.toString().split('#')[1]
        }">${icon}</use>
          </svg>
        `;
        el.append(svg);
      }
    });
  });
});
